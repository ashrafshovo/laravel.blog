<?php 

// database connection info
$host="localhost";
$user="root";
$pass="";
$db="laravel_blog";

// established database connection
$mysqli=new mysqli($host,$user,$pass,$db);

// select posts
$sql="select p.id as id, p.title as title, p.text as text, p.created as created, a.id as author_id, a.fname as firstname, a.lname as lastname from posts p, author as a where p.author_id=a.id order by created desc";

//query database and store result for use
$result=$mysqli->query($sql,MYSQLI_STORE_RESULT);

?>

<!DOCTYPE html>
<html lang="en">
<head>
	<title>Laravel Blog</title>

	<!--- Bootstrap stylesheet -->
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<!--- to be responsive -->
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-responsive.css">
</head>

<body>
	<div class="container">
		
		<!--- top navigation bar -->
		<div class="navbar">
			<div class="navbar-inner">
			<a class="navbar-brand" href="#">LaravelBlog</a>
				<ul class="nav nav-pills">
					<li class="active"><a href="#">Home</a></li>
					<li><a href="#">About</a></li>
					<li><a href="#">Categories</a></li>
				</ul>
			</div>
		</div>

		<!-- main body -->
		<div class="row-fluid">
			<!-- content area -->
			<div class="span9">
				<h1>Welcome to my blog!</h1>
				<h1>আমার ব্লগে আপনাকে স্বাগতম!</h2>

				<?php 

				while ($post=$result->fetch_object()) {
					# code...
					echo '<div class="post">';
					echo '<h3>'.$post->title.'</h3>';
					echo '<address> Written by: '.$post->firstname.' '.$post->lastname.', posted on: '.$post->created.'</address>';
					echo '<p>'.$post->text.'</p>';
					echo '<hr />';
					echo '</div>';
				}
				?>
			</div>
			<!-- side bar -->
			<div class="span3">
				<h2>Sidebar</h2>
			</div>
		</div>

		<!-- footer -->
		<div class="span12">
			Copyright &copy 2005-2017. Ashraf Hossan Shovo. All rights reserved.
		</div>

	</div>

	<!-- bootstrap js -->
	<script type="text/javascript" src="js/bootstrap.min.js"></script>
</body>
</html>