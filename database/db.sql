create table if not exists author (
id int(11) not null auto_increment,
fname varchar(100) not null,
lname varchar(100) not null,
email varchar(100) not null,
username varchar(30) not null,
twitter varchar(30) not null,
web varchar(200) not null,
password varchar(200) not null,
profile text not null,
primary key (id)
)
engine=MyISAM default charset=utf8;


INSERT INTO author
	values(1, 'Borhan', 'Uddin', 'borhan@ashrafshovo.com','borhan', 'borhan', 'www.borhan.me','',
		'I am a test writer.'),
		(2, 'Ashraf', 'Shovo', 'a.shovo@ashrafshovo.com','ashraf', 'ashraf', 'www.ashraf.me','',
			'I am a php developer and the creator of this blog.');

create table if not exists posts (
id int(11) not null auto_increment,
title varchar(255) not null,
text text not null,
author_id int(11) not null,
created datetime not null,
modified datetime not null,
primary key(id)
)
engine=MyISAM default charset=utf8;


INSERT INTO posts
	values(1, 'What is PHP?', 'PHP (recursive accronym for PHP: Hypertext Preprocessor) is a widely-used open source general-purpose scripting language that is specially suited for web development and can be embaded into HTML.\r\nWhat dishtinguishes PHP from something like client-side javascriptis that the code is executed on server, generating HTML which is than sent to the client. The client would receive the results of running that script, but would not know what the underlying code was. You can even configure your web server to process all your HTML files with PHP, and than there''s really no way that users can tell what you have up your sleeve.\r\n\r\nThe best things in using PHP are that is extremely simple for a newcomer, but offers many advanced features for a professional programmer. Don''t be afraid reading the long list of PHP''s features. You can jump in, in a short time, and started writing simple scripts in a few houres.',2,'2017-01-08 00:00:00','2017-01-08 00:00:00'),
		(2, 'What is Joomla?','Joomla! is a award wining content management system. It is used for different types of wweb applications. Joomla! is highly extensible and you can extend its functionality by using extension. There are five types of extension - templetes, components, modules, plugins and lamguages.\r\n\r\nYou can get a free copy of Joomla! from http://www.joomla.org',1,'2017-02-21 00:00:00','2017-02-21 00:00:00'),
		(3,'What is Laravel?','Laravel is a free, open-source PHP web framework, created by Taylor Otwell and intended for the development of web applications following the model–view–controller (MVC) architectural pattern.',2,'2017-06-05 00:00:00','2017-06-05 00:00:00');
