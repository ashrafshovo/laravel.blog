<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, SparkPost and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),             // Your Stripe Key
        'secret' => env('STRIPE_SECRET'),       // Your Stripe Secret
    ],

    'github' => [
        'client_id' => env('GITHUB_ID'),         // Your GitHub Client ID
        'client_secret' => env('GITHUB_SECRET'), // Your GitHub Client Secret
        'redirect' => env('GITHUB_URL'),         // Github Callback URL
    ],

    'twitter' => [
        'client_id' => env('TWITTER_ID'),         // Your Twitter Client ID
        'client_secret' => env('TWITTER_SECRET'), // Your Twitter Client Secret
        'redirect' => env('TWITTER_URL'),         // Twitter Callback URL
    ],

    'facebook' => [
        'client_id' => env('FACEBOOK_ID'),         // Your Facebook Client ID
        'client_secret' => env('FACEBOOK_SECRET'), // Your Facebook Client Secret
        'redirect' => env('FACEBOOK_URL'),         // Facebook Callback URL
    ],

    'google' => [
        'client_id' => env('GOOGLE_ID'),         // Your Twitter Client ID
        'client_secret' => env('GOOGLE_SECRET'), // Your Twitter Client Secret
        'redirect' => env('GOOGLE_URL'),         // Google Callback URL
    ],

    'linkedin' => [
        'client_id' => env('LINKEDIN_ID'),         // Your LinkedIn Client ID
        'client_secret' => env('LINKEDIN_SECRET'), // Your LinkedIn Client Secret
        'redirect' => env('LINKEDIN_URL'),         // LinkedIn Callback URL
    ],

    'linkedin' => [
        'client_id' => env('GITLAB_ID'),         // Your GitLab Client ID
        'client_secret' => env('GITLAB_SECRET'), // Your GitLab Client Secret
        'redirect' => env('GITLAB_URL'),         // GitLab Callback URL
    ],

];
