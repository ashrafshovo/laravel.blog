<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Laravel\Socialite\Facades\Socialite;
use App\User;


class SocialAuthController extends Controller
{
    //

    public function redirectToProvider($provider)
    {

        if ($provider == 'gitlab' || $provider == 'yaho')
        {
            return Socialite::$provider->redirect();
        }

        return Socialite::driver($provider)->redirect();
    }

    public function handleProviderCallback ($provider)
    {
        // get the user

        if ($provider == 'twitter') {
            $user = Socialite::driver($provider)->user();                // google does not work, but twitter work

            //store user info

            $authUser=User::firstOrNew(['provider_id'=> $user->id]);

            $authUser->name=$user->name;
            $authUser->email=$user->email;
            $authUser->provider=$provider;
            $authUser->provider_id=$user->id;
            //$authUser->avatar=$user->avatar;

            $authUser->save();


            auth()->login($authUser);
        }

        else {
            $user = Socialite::driver($provider)->stateless()->user();   // twitter does not work, but google work

            //store user info

            $authUser=User::firstOrNew(['provider_id'=> $user->id]);

            $authUser->name=$user->name;
            $authUser->email=$user->email;
            $authUser->provider=$provider;
            $authUser->provider_id=$user->id;
            //$authUser->pro_pic=$user->avatar_original;

            $authUser->save();


            auth()->login($authUser);
        }


        //dd($user);

        return redirect('/home');

    }

}
